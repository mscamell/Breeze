# README #

minSDK = 16

maxSDK = 23

### How I built the app ###

I've used a combination of an MVP pattern with Dagger 2 for dependency injection. The app uses Retrofit for network calls but this should be easy to swap out if another networking library was needed. The same goes for Realm which is used as part of the database. I've added a few tests using Espresso for the HomeActivity and a few unit tests for the StringUtils class.

The app itself is pretty functional. It attempts to refresh the wind forecasts on each open and notifies the user if their phone is offline. I decided on using images to indicate how windy it is currently for each favourite city. It's only a crude implementation with 3 different images but I imagine this could be changed to utilise images from an API that could be more fine grained and different over a period of time to keep it fresh. The cards on the home screen provide a nice container to present both the image and the current wind speed and direction. The FAB button provides the ability to add a city and also removes itself on scroll down via a behaviour so that it does not block the wind direction/speed of the items at the bottom of the list. The favourite cities can be cleared via the overflow menu.

The forecast screen uses 5 tabs to represent each day forecasted. Within each tab is a list of the forecasted hours for that day in time order. It's quite simple but easy to look at.

The app should work on tablets but it's not been tested as I don't own one.

### What could be improved/is missing ###

* More tests, potentially utilising Roboelectric and Dagger 2 for mock objects
* The ability to remove individual favourite cities
* Pull to refresh (requires some boilerplate with Realm) on the home screen and forecast screens
* More images for the different wind speeds
* Notifications of the last time the app was refreshed
* Something to rate limit to prevent people spamming the API. I imagine storing and utilising the last successful update and then going off of that.
* A more pleasing looking UI on the forecast screen.
* A UI that is better adapted for UI use by potential use of master/detail view and different dimension values and layouts
* More validation on the fav cities entered in the edit text, but if I had more time I'd like to find a way to offer the same way of adding cities as the api e.g. lat long. For now the API should refuse if it cannot find the city based on what's entered.