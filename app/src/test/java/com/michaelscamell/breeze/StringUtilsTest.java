package com.michaelscamell.breeze;

import com.michaelscamell.breeze.utils.StringUtils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class StringUtilsTest {

    private ArrayList<Integer> cityIds;
    private String formattedString;

    @Before
    public void setup() {
        cityIds = new ArrayList<>();
    }

    @Test
    public void cityIdsStringFormatter_ValidIds_ReturnsCorrectlyFormattedString() {
        cityIds.add(123);
        cityIds.add(4567);
        cityIds.add(7891011);

        formattedString = StringUtils.cityIdsStringFormatter(cityIds);
        Assert.assertTrue(formattedString.equals("123,4567,7891011"));
    }

    @Test
    public void cityIdsStringFormatter_EmptyArray_ReturnsEmptyString() {
        formattedString = StringUtils.cityIdsStringFormatter(cityIds);
        Assert.assertTrue(formattedString.equals(""));
    }

    @Test
    public void speedWithMPH_WithFloatNumber_ReturnsCorrectlyFormattedString() {
        formattedString = StringUtils.speedWithMPH(8.9f);
        Assert.assertTrue(formattedString.equals("8.9mph"));
    }

    @Test
    public void speedWithMPH_WithIntegerNumber_ReturnsCorrectlyFormattedString() {
        formattedString = StringUtils.speedWithMPH(34f);
        Assert.assertTrue(formattedString.equals("34mph"));
    }

    @Test
    public void speedWithMPH_With0Number_ReturnsCorrectlyFormattedString() {
        formattedString = StringUtils.speedWithMPH(0f);
        Assert.assertTrue(formattedString.equals("0mph"));
    }

    @Test
    public void getForecastTime_WithCorrectDateText_ReturnsCorrectlyFormattedString() {
        formattedString = StringUtils.getForecastTime("2014-07-23 09:00:00");
        Assert.assertTrue(formattedString.equals("09:00"));
    }

    @Test
    public void getForecastDate_WithCorrectDateText_ReturnsCorrectlyFormattedString() {
        formattedString = StringUtils.getForecastDate("2014-07-23 09:00:00");
        Assert.assertTrue(formattedString.equals("23/07"));
    }
}
