package com.michaelscamell.breeze.application;

import android.app.Application;

import com.michaelscamell.breeze.data.api.APIClient;
import com.michaelscamell.breeze.data.api.APIModule;
import com.michaelscamell.breeze.data.local.DataManager;
import com.michaelscamell.breeze.data.local.DataModule;
import com.michaelscamell.breeze.utils.NetworkUtils;
import com.michaelscamell.breeze.utils.PostFromAnyThreadBus;

import javax.inject.Singleton;

import dagger.Component;
import io.realm.RealmConfiguration;

/**
 * The Application component
 */
@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                DataModule.class,
                APIModule.class
        }
)
public interface ApplicationComponent {
    void inject(BaseApplication companionApplication);

    Application application();

    APIClient apiClient();

    DataManager dataManager();

    RealmConfiguration realmConfiguration();

    PostFromAnyThreadBus bus();

    NetworkUtils networkUtils();

    /**
     * The type Initialiser.
     */
    final class Initialiser {
        private Initialiser() {
        }

        static ApplicationComponent init(BaseApplication baseApplication) {
            return DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(baseApplication))
                    .build();
        }
    }
}

