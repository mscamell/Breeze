package com.michaelscamell.breeze.application;

import android.app.Application;

import com.michaelscamell.breeze.utils.NetworkUtils;
import com.michaelscamell.breeze.utils.PostFromAnyThreadBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Provides application wide dependencies
 */
@Module
public class ApplicationModule {

    private final BaseApplication app;

    /**
     * Instantiates a new Application module.
     *
     * @param app the app
     */
    public ApplicationModule(final BaseApplication app) {
        this.app = app;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return app;
    }

    @Provides
    @Singleton
    PostFromAnyThreadBus provideBus() {
        return new PostFromAnyThreadBus();
    }

    @Provides
    @Singleton
    NetworkUtils provideNetworkUtils(Application context) {
        return new NetworkUtils(context);
    }
}