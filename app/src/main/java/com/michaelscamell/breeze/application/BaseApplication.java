package com.michaelscamell.breeze.application;

import android.app.Application;
import android.content.Context;

/**
 * Used for setup on application launch
 */
public class BaseApplication extends Application {

    private ApplicationComponent component;

    public static BaseApplication get(Context context) {
        return (BaseApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        buildComponentAndInject();
    }

    /**
     * Intitialises and injects the Application component
     */
    private void buildComponentAndInject() {
        component = ApplicationComponent.Initialiser.init(this);
        component.inject(this);
    }

    public ApplicationComponent component() {
        return component;
    }
}