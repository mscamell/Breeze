package com.michaelscamell.breeze.utils;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import timber.log.Timber;

public class StringUtils {

    private static final String COMMA = ",";
    private static final String MPH = "mph";

    public static String cityIdsStringFormatter(@NonNull ArrayList<Integer> cityIds) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < cityIds.size(); i++) {
            sb.append(cityIds.get(i));
            if (i + 1 != cityIds.size()) sb.append(COMMA);
        }
        return sb.toString();
    }

    public static String speedWithMPH(float speed) {
        DecimalFormat df = new DecimalFormat("#.#");
        return df.format(speed) + MPH;
    }

    @SuppressLint("SimpleDateFormat")
    public static String getForecastTime(String dateText) {
        Date date;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateText);
        } catch (ParseException e) {
            Timber.e("Couldn't parse date text. Exception: %s", e.toString());
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public static String getForecastDate(String dateText) {
        Date date;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateText);
        } catch (ParseException e) {
            Timber.e("Couldn't parse date text. Exception: %s", e.toString());
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM", Locale.getDefault());
        return sdf.format(date);
    }
}
