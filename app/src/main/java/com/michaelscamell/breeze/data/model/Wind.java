package com.michaelscamell.breeze.data.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * The Wind data model.
 */
public class Wind extends RealmObject {

    @SerializedName("speed")
    private float speed;
    @SerializedName("deg")
    private float degrees;

    /**
     * Gets degrees.
     *
     * @return the degrees
     */
    public float getDegrees() {
        return degrees;
    }

    /**
     * Sets degrees.
     *
     * @param degrees the degrees
     */
    public void setDegrees(float degrees) {
        this.degrees = degrees;
    }

    /**
     * Gets speed.
     *
     * @return the speed
     */
    public float getSpeed() {
        return this.speed;
    }

    /**
     * Sets speed.
     *
     * @param speed the speed
     */
    public void setSpeed(float speed) {
        this.speed = speed;
    }
}