package com.michaelscamell.breeze.data.api;

import java.util.ArrayList;

/**
 * The interface to be implemented by any type of API Client
 */
public interface APIClient {

    /**
     * Gets current wind condition for a city.
     *
     * @param cityName the city name
     */
    void getCurrentWindCondition(String cityName);

    /**
     * Gets current wind conditions for a group of cities.
     *
     * @param cityIds the city ids
     */
    void getCurrentWindConditions(ArrayList<Integer> cityIds);

    /**
     * Gets forecast weather conditions for a city.
     *
     * @param cityId the city id
     */
    void getForecastWeatherConditions(int cityId);

}
