package com.michaelscamell.breeze.data.api;

import android.support.annotation.NonNull;

import com.michaelscamell.breeze.data.api.response.CurrentWeatherResponse;
import com.michaelscamell.breeze.data.api.response.ForecastResponse;
import com.michaelscamell.breeze.data.api.response.ForecastWindResponse;
import com.michaelscamell.breeze.data.api.response.GroupCurrentWeatherResponse;
import com.michaelscamell.breeze.data.local.DataManager;
import com.michaelscamell.breeze.data.model.City;
import com.michaelscamell.breeze.data.model.ForecastWind;
import com.michaelscamell.breeze.events.ForecastWeatherConditionsSuccessEvent;
import com.michaelscamell.breeze.events.failed.APICallFailEvent;
import com.michaelscamell.breeze.utils.PostFromAnyThreadBus;
import com.michaelscamell.breeze.utils.StringUtils;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * A Retrofit API Client implementation
 */
public class RetrofitAPIClient implements APIClient {

    public static final String IMPERIAL = "imperial";

    private final OpenWeatherMapService openWeatherAPIService;
    private final DataManager dataManager;
    private final PostFromAnyThreadBus bus;

    @Inject
    public RetrofitAPIClient(OpenWeatherMapService openWeatherAPIService, DataManager dataManager, PostFromAnyThreadBus bus) {
        this.openWeatherAPIService = openWeatherAPIService;
        this.dataManager = dataManager;
        this.bus = bus;
    }

    @Override
    public void getCurrentWindCondition(final String cityName) {
        openWeatherAPIService.getCurrentWindCondition(cityName, OpenWeatherMapService.AUTHORISATION, IMPERIAL)
                .enqueue(new Callback<CurrentWeatherResponse>() {
                    @Override
                    public void onResponse(Call<CurrentWeatherResponse> call, Response<CurrentWeatherResponse> response) {
                        if (response.isSuccessful()) {
                            Timber.d("Successfully retrieved current wind conditions for %s", cityName);
                            City city = mapCity(response.body());
                            dataManager.addFavourite(city);

                            //Once we have the favourite city saved make a call to
                            // get forecast weather conditions
                            getForecastWeatherConditions(city.getCityId());
                        } else {
                            Timber.d("Failed to get current wind conditions for %s", cityName);
                            Timber.d("Error status: %d", response.code());
                            //if there was an error than post an event to let subscribers know the
                            //call failed
                            bus.post(new APICallFailEvent());
                        }
                    }

                    @Override
                    public void onFailure(Call<CurrentWeatherResponse> call, Throwable t) {
                        Timber.w("Call failed getting current wind conditions for %s", cityName);
                        Timber.w("Call failure error: %s", t.getCause().toString());
                        //for any other kind of failure post a an event with no response code
                        bus.post(new APICallFailEvent());
                    }
                });
    }


    @Override
    public void getCurrentWindConditions(final ArrayList<Integer> cityIds) {
        String cityIdsString = StringUtils.cityIdsStringFormatter(cityIds);
        openWeatherAPIService.getCurrentWeatherConditions(cityIdsString, OpenWeatherMapService.AUTHORISATION, IMPERIAL)
                .enqueue(new Callback<GroupCurrentWeatherResponse>() {
                    @Override
                    public void onResponse(Call<GroupCurrentWeatherResponse> call, Response<GroupCurrentWeatherResponse> response) {
                        if (response.isSuccessful()) {
                            Timber.d("Successfully retrieved current wind conditions for cities %s", cityIds.toString());

                            // Get the response and map them all to city data models and update the database
                            if (response.body().getCitiesCurrentWindConditions() != null) {
                                for (CurrentWeatherResponse cityResponse : response.body().getCitiesCurrentWindConditions()) {
                                    City city = mapCity(cityResponse);
                                    dataManager.updateCurrentWindCondition(city);
                                }
                            }
                        } else {
                            Timber.d("Failed to get current wind conditions for cities %s", cityIds.toString());
                            Timber.d("Error status: %d", response.code());
                            bus.post(new APICallFailEvent());
                        }
                    }

                    @Override
                    public void onFailure(Call<GroupCurrentWeatherResponse> call, Throwable t) {
                        Timber.w("Call failed getting current wind conditions for cities %s", cityIds.toString());
                        Timber.w("Call failure error: %s", t.getCause().toString());
                        bus.post(new APICallFailEvent());
                    }
                });
    }

    @Override
    public void getForecastWeatherConditions(final int cityId) {
        openWeatherAPIService.getForecastWindConditions(cityId, OpenWeatherMapService.AUTHORISATION, IMPERIAL)
                .enqueue(new Callback<ForecastResponse>() {
                    @Override
                    public void onResponse(Call<ForecastResponse> call, Response<ForecastResponse> response) {
                        if (response.isSuccessful()) {
                            Timber.d("Successfully retrieved forecast wind conditions for city %d", cityId);

                            //get the response and may the ForecastWindResponses to the Forecast winds so we can more easily
                            //use dates and times as well as easier sorting
                            ArrayList<ForecastWind> forecastWinds = new ArrayList<>();
                            if (response.body().getForecastWindConditions() != null) {
                                for (ForecastWindResponse forecastWindResponse : response.body().getForecastWindConditions()) {
                                    ForecastWind forecastWind = new ForecastWind();
                                    forecastWind.setForecastWind(forecastWindResponse.getForecastWind());
                                    forecastWind.setForecastDate(StringUtils.getForecastDate(forecastWindResponse.getForecastDate()));
                                    forecastWind.setForecastTime(StringUtils.getForecastTime(forecastWindResponse.getForecastDate()));
                                    forecastWinds.add(forecastWind);
                                }

                                //Add the forecast weather to the city in the database
                                dataManager.addForecastWindConditions(cityId, forecastWinds);

                                //Let any subscribers know the call has been successful
                                bus.post(new ForecastWeatherConditionsSuccessEvent());
                            }
                        } else {
                            Timber.d("Failed to get forecast wind conditions for city %d", cityId);
                            Timber.d("Error status: %d", response.code());
                            bus.post(new APICallFailEvent());
                        }
                    }

                    @Override
                    public void onFailure(Call<ForecastResponse> call, Throwable t) {
                        Timber.w("Call failed getting forecast wind conditions for city %d", cityId);
                        Timber.w("Call failure error: %s", t.getCause().toString());
                        bus.post(new APICallFailEvent());
                    }
                });
    }

    /**
     * If there was more time this could be part of a mapper class that maps response objects to data
     * models
     *
     * @param responseCity the response city
     * @return a city data model
     */
    @NonNull
    private City mapCity(CurrentWeatherResponse responseCity) {
        City city = new City();
        city.setCityId(responseCity.getCityId());
        city.setCityName(responseCity.getCityName());
        city.setCurrentWindConditions(responseCity.getWind());
        city.setDate(responseCity.getDate());
        return city;
    }
}
