package com.michaelscamell.breeze.data.local;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;

@Module
public class DataModule {

    public static final int SCHEMA_VERSION = 0;

    @Provides
    @Singleton
    RealmConfiguration provideRealmUserConfiguration(Application application) {
        return new RealmConfiguration.Builder(application)
                .schemaVersion(SCHEMA_VERSION)
                .build();
    }

    @Provides
    @Singleton
    Realm provideRealm(RealmConfiguration realmConfiguration) {
        Realm.setDefaultConfiguration(realmConfiguration);
        return Realm.getInstance(realmConfiguration);
    }

    @Provides
    @Singleton
    DataManager provideDataManager(Realm realm) {
        return new RealmDataManager(realm);
    }
}
