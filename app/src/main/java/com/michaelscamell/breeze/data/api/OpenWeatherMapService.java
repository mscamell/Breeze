package com.michaelscamell.breeze.data.api;

import com.michaelscamell.breeze.data.api.response.CurrentWeatherResponse;
import com.michaelscamell.breeze.data.api.response.ForecastResponse;
import com.michaelscamell.breeze.data.api.response.GroupCurrentWeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenWeatherMapService {

    String AUTHORISATION = "82ad1488cace2bc54e0e11443366a124";
    String APPID = "APPID";
    String UNITS = "units";

    @GET("weather")
    Call<CurrentWeatherResponse> getCurrentWindCondition(@Query("q") String cityName, @Query(APPID) String appId, @Query(UNITS) String units);

    @GET("group")
    Call<GroupCurrentWeatherResponse> getCurrentWeatherConditions(@Query("id") String cityIds, @Query(APPID) String appId, @Query(UNITS) String units);

    @GET("forecast")
    Call<ForecastResponse> getForecastWindConditions(@Query("id") int cityId, @Query(APPID) String appId, @Query(UNITS) String units);

}
