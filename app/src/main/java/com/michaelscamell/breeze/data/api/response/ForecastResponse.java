package com.michaelscamell.breeze.data.api.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The response received when getting the forecast for a specified city
 */
public class ForecastResponse {

    @SerializedName("list")
    private List<ForecastWindResponse> forecastWindConditions;

    /**
     * Gets the forecast wind conditions.
     *
     * @return the forecast wind conditions
     */
    public List<ForecastWindResponse> getForecastWindConditions() {
        return forecastWindConditions;
    }

    /**
     * Sets the forecast wind conditions.
     *
     * @param forecastWindConditions the forecast wind conditions
     */
    public void setForecastWindConditions(List<ForecastWindResponse> forecastWindConditions) {
        this.forecastWindConditions = forecastWindConditions;
    }
}
