package com.michaelscamell.breeze.data.local;

import com.michaelscamell.breeze.data.model.City;
import com.michaelscamell.breeze.data.model.ForecastWind;

import java.util.List;

/**
 * Defines the methods that should be implemented by whatever is chosen to manage
 * local data
 */
public interface DataManager {

    /**
     * Add favourite.
     *
     * @param city the city
     */
    void addFavourite(City city);

    /**
     * Gets a favourite city.
     *
     * @param cityId the city id
     * @return the favourite
     */
    City getFavourite(int cityId);

    /**
     * Gets a favourite city.
     *
     * @param cityName the city name
     * @return the favourite
     */
    City getFavourite(String cityName);

    /**
     * Gets all favourite cities.
     *
     * @return the favourites
     */
    List<City> getFavourites();

    /**
     * Add forecast wind conditions.
     *
     * @param cityId                 the city id
     * @param forecastWindConditions the forecast wind conditions
     */
    void addForecastWindConditions(int cityId, List<ForecastWind> forecastWindConditions);

    /**
     * Update current wind condition.
     *
     * @param city the city
     */
    void updateCurrentWindCondition(City city);

    /**
     * Gets forecast wind conditions.
     *
     * @param cityName the city name
     * @return the forecast wind conditions
     */
    List<ForecastWind> getForecastWindConditions(String cityName);


    /**
     * Gets forecast wind conditions for a specific date
     *
     * @param cityName the city name
     * @param date     the date formatted by day month e.g. 24/05
     * @return the forecast wind conditions
     */
    List<ForecastWind> getForecastWindConditions(String cityName, String date);

    /**
     * Clears all favourite cities.
     */
    void clearFavouriteCities();
}
