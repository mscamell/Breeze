package com.michaelscamell.breeze.data.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * The City data model.
 */
public class City extends RealmObject {

    @PrimaryKey
    private int cityId;
    private String cityName;
    private Wind currentWindConditions;
    private RealmList<ForecastWind> forecastWindConditions;
    private int date;

    /**
     * Gets current wind conditions.
     *
     * @return the current wind conditions
     */
    public Wind getCurrentWindConditions() {
        return currentWindConditions;
    }

    /**
     * Sets current wind conditions.
     *
     * @param currentWindConditions the current wind conditions
     */
    public void setCurrentWindConditions(Wind currentWindConditions) {
        this.currentWindConditions = currentWindConditions;
    }

    /**
     * Gets forecast wind conditions.
     *
     * @return the forecast wind conditions
     */
    public RealmList<ForecastWind> getForecastWindConditions() {
        return forecastWindConditions;
    }

    /**
     * Sets forecast wind conditions.
     *
     * @param forecastWindConditions the forecast wind conditions
     */
    public void setForecastWindConditions(RealmList<ForecastWind> forecastWindConditions) {
        this.forecastWindConditions = forecastWindConditions;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public int getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(int date) {
        this.date = date;
    }

    /**
     * Gets city id.
     *
     * @return the city id
     */
    public int getCityId() {
        return cityId;
    }

    /**
     * Sets city id.
     *
     * @param cityId the city id
     */
    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    /**
     * Gets city name.
     *
     * @return the city name
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * Sets city name.
     *
     * @param cityName the city name
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
