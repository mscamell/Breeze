package com.michaelscamell.breeze.data.local;

import com.michaelscamell.breeze.data.model.City;
import com.michaelscamell.breeze.data.model.ForecastWind;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import timber.log.Timber;

public class RealmDataManager implements DataManager {

    private final Realm realm;

    @Inject
    public RealmDataManager(Realm realm) {
        this.realm = realm;
    }

    @Override
    public void addFavourite(City city) {
        Timber.i("Adding favourite city %s", city.getCityName());
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(city);
        realm.commitTransaction();
    }

    @Override
    public City getFavourite(int cityId) {
        Timber.i("Getting favourite city %s", cityId);
        return realm.where(City.class).equalTo("cityId", cityId).findFirst();
    }

    @Override
    public City getFavourite(String cityName) {
        Timber.i("Getting favourite city %s", cityName);
        return realm.where(City.class).equalTo("cityName", cityName).findFirst();
    }

    @Override
    public List<City> getFavourites() {
        Timber.i("Getting favourite cities");
        return realm.where(City.class).findAll();
    }

    @Override
    public void addForecastWindConditions(int cityId, List<ForecastWind> forecastWindConditions) {
        Timber.i("Adding forecast wind conditions for %s", cityId);
        realm.beginTransaction();
        City city = getFavourite(cityId);
        for (int i = 0; i < forecastWindConditions.size(); i++) {
            city.getForecastWindConditions().add(forecastWindConditions.get(i));
        }
        realm.commitTransaction();
    }

    @Override
    public void updateCurrentWindCondition(City city) {
        Timber.i("Updating current wind conditions for %s", city.getCityName());
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(city);
        realm.commitTransaction();
    }

    @Override
    public List<ForecastWind> getForecastWindConditions(String cityName) {
        Timber.i("Getting forecast wind conditions for %s", cityName);
        return getFavourite(cityName).getForecastWindConditions();
    }

    @Override
    public List<ForecastWind> getForecastWindConditions(String cityName, String date) {
        Timber.i("Getting forecast wind conditions for %s for the %s", cityName, date);
        return getFavourite(cityName).getForecastWindConditions().where().equalTo("forecastDate", date).findAll();
    }

    @Override
    public void clearFavouriteCities() {
        Timber.i("Clearing favourite cities from the database");
        realm.beginTransaction();
        realm.clear(City.class);
        realm.commitTransaction();
    }

}