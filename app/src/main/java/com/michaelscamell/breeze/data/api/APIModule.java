package com.michaelscamell.breeze.data.api;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.michaelscamell.breeze.data.local.DataManager;
import com.michaelscamell.breeze.utils.PostFromAnyThreadBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.RealmObject;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * The module for API related dependencies
 */
@Module
public class APIModule {

    private static final String API_BASE_URL = "http://api.openweathermap.org/data/2.5/";

    /**
     * Provides the OpenWeather API Service
     *
     * @return the OpenWeather API Service
     */
    @Provides
    @Singleton
    OpenWeatherMapService provideOpenWeatherAPIService() {
        return new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(buildGsonForRealm()))
                .build()
                .create(OpenWeatherMapService.class);
    }

    /**
     * Provides an implementation of the APIClient
     *
     * @param openWeatherAPIService used to create an implementation using Retrofit
     * @return An API Client implementation
     */
    @Provides
    @Singleton
    APIClient provideAPIClient(OpenWeatherMapService openWeatherAPIService, DataManager dataManager, PostFromAnyThreadBus bus) {
        return new RetrofitAPIClient(openWeatherAPIService, dataManager, bus);
    }

    /**
     * Specific Gson build so we can use objects from Retrofit in Realm
     *
     * @return a Realm Gson build
     */
    private Gson buildGsonForRealm() {
        return new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                }).create();
    }
}

