package com.michaelscamell.breeze.data.api.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The response received when asking for a group of cities current weather conditions.
 * We only take the Wind conditions as that is all we currently need.
 */
public class GroupCurrentWeatherResponse {

    @SerializedName("cnt")
    private int count;
    @SerializedName("list")
    private List<CurrentWeatherResponse> citiesCurrentWindConditions;

    /**
     * Gets count.
     *
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * Sets count.
     *
     * @param count the count
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Gets cities current wind conditions.
     *
     * @return the cities current wind conditions
     */
    public List<CurrentWeatherResponse> getCitiesCurrentWindConditions() {
        return citiesCurrentWindConditions;
    }

    /**
     * Sets cities current wind conditions.
     *
     * @param citiesCurrentWindConditions the cities current wind conditions
     */
    public void setCitiesCurrentWindConditions(List<CurrentWeatherResponse> citiesCurrentWindConditions) {
        this.citiesCurrentWindConditions = citiesCurrentWindConditions;
    }
}

