package com.michaelscamell.breeze.data.model;

import io.realm.RealmObject;

/**
 * The Forecast Wind data model. Currently cannot extend classes that extend RealmObject otherwise
 * we could have reused the Wind data model
 */
public class ForecastWind extends RealmObject {

    private String forecastDate;
    private String forecastTime;
    private Wind forecastWind;

    /**
     * Gets forecast wind.
     *
     * @return the forecast wind
     */
    public Wind getForecastWind() {
        return forecastWind;
    }

    /**
     * Sets forecast wind.
     *
     * @param forecastWind the forecast wind
     */
    public void setForecastWind(Wind forecastWind) {
        this.forecastWind = forecastWind;
    }

    /**
     * Gets forecast date.
     *
     * @return the forecast date
     */
    public String getForecastDate() {
        return forecastDate;
    }

    /**
     * Sets forecast date.
     *
     * @param forecastDate the forecast date
     */
    public void setForecastDate(String forecastDate) {
        this.forecastDate = forecastDate;
    }

    /**
     * Gets forecast time.
     *
     * @return the forecast time
     */
    public String getForecastTime() {
        return forecastTime;
    }

    /**
     * Sets forecast time.
     *
     * @param forecastTime the forecast time
     */
    public void setForecastTime(String forecastTime) {
        this.forecastTime = forecastTime;
    }
}
