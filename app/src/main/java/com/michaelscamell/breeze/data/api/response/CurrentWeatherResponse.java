package com.michaelscamell.breeze.data.api.response;

import com.google.gson.annotations.SerializedName;
import com.michaelscamell.breeze.data.model.Wind;

/**
 * The response received when asking for the current weather conditions of a
 * single city. This only takes the Wind conditions as it's the only condition
 * needed currently.
 */
public class CurrentWeatherResponse {

    @SerializedName("wind")
    private Wind wind;
    @SerializedName("dt")
    private int date;
    @SerializedName("id")
    private int cityId;
    @SerializedName("name")
    private String cityName;

    /**
     * Gets wind.
     *
     * @return the wind
     */
    public Wind getWind() {
        return wind;
    }

    /**
     * Sets wind.
     *
     * @param wind the wind
     */
    public void setWind(Wind wind) {
        this.wind = wind;
    }

    /**
     * Gets date.
     *
     * @return the date
     */
    public int getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date the date
     */
    public void setDate(int date) {
        this.date = date;
    }

    /**
     * Gets city id.
     *
     * @return the city id
     */
    public int getCityId() {
        return cityId;
    }

    /**
     * Sets city id.
     *
     * @param cityId the city id
     */
    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    /**
     * Gets city name.
     *
     * @return the city name
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * Sets city name.
     *
     * @param cityName the city name
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}