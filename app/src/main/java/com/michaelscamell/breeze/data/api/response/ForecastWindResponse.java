package com.michaelscamell.breeze.data.api.response;

import com.google.gson.annotations.SerializedName;
import com.michaelscamell.breeze.data.model.Wind;

/**
 * The Forecast Wind data model. Currently cannot extend classes that extend RealmObject otherwise
 * we could have reused the Wind data model
 */
public class ForecastWindResponse {

    @SerializedName("wind")
    private Wind forecastWind;
    @SerializedName("dt_txt")
    private String forecastDate;

    /**
     * Gets forecast wind.
     *
     * @return the forecast wind
     */
    public Wind getForecastWind() {
        return forecastWind;
    }

    /**
     * Sets forecast wind.
     *
     * @param forecastWind the forecast wind
     */
    public void setForecastWind(Wind forecastWind) {
        this.forecastWind = forecastWind;
    }

    /**
     * Gets forecast date.
     *
     * @return the forecast date
     */
    public String getForecastDate() {
        return forecastDate;
    }

    /**
     * Sets forecast date.
     *
     * @param forecastDate the forecast date
     */
    public void setForecastDate(String forecastDate) {
        this.forecastDate = forecastDate;
    }

}
