package com.michaelscamell.breeze.ui.common.LifeCycle;

public interface Create {
    void onCreate();
}
