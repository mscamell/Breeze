package com.michaelscamell.breeze.ui.home;

import com.michaelscamell.breeze.data.model.City;
import com.michaelscamell.breeze.ui.common.BaseView;

import java.util.List;

public interface HomeView extends BaseView {

    void showLoading();

    void hideLoading();

    void setupRecyclerView();

    void updateFavouriteCities(List<City> cities);

    void notifyDataSetChanged();

    void hideFavouriteCitiesList();

    void hideEmptyListMessage();

    void showEmptyListMessage();

    void showFavouriteCitiesList();

    void showAddFavCityErrorMessage();

    void showOfflineMessage();
}

