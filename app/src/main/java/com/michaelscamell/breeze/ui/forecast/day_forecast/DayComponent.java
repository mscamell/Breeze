package com.michaelscamell.breeze.ui.forecast.day_forecast;

import com.michaelscamell.breeze.application.ApplicationComponent;
import com.michaelscamell.breeze.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(
        dependencies = ApplicationComponent.class,
        modules = DayModule.class
)
public interface DayComponent {
    void inject(DayFragment dayFragment);
}
