package com.michaelscamell.breeze.ui.home;

/**
 * Listener used when entering favourite city
 */
public interface AddFavouriteCityListener {

    /**
     * On city entered.
     *
     * @param city the city
     */
    void onCityEntered(String city);
}
