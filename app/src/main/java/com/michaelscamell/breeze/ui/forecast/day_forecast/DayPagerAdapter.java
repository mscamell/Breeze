package com.michaelscamell.breeze.ui.forecast.day_forecast;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.michaelscamell.breeze.ui.forecast.ForecastActivity;

import java.util.List;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class DayPagerAdapter extends FragmentPagerAdapter {

    private List<DayFragment> dayFragments;
    private List<String> tabTitles;

    public DayPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public DayPagerAdapter(FragmentManager fm, List<DayFragment> dayFragments, List<String> tabTitles) {
        super(fm);
        this.dayFragments = dayFragments;
        this.tabTitles = tabTitles;
    }

    @Override
    public Fragment getItem(int position) {
        return dayFragments.get(position);
    }

    @Override
    public int getCount() {
        return ForecastActivity.FORECAST_LENGTH;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles.get(position);
    }
}
