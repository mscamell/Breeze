package com.michaelscamell.breeze.ui.common;

public abstract class BasePresenter<T extends BaseView> {

    protected T view;

    protected BasePresenter(final T view) {
        this.view = view;
    }

}

