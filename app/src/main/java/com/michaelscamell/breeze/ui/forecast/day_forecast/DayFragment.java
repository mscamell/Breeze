package com.michaelscamell.breeze.ui.forecast.day_forecast;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.michaelscamell.breeze.R;
import com.michaelscamell.breeze.application.ApplicationComponent;
import com.michaelscamell.breeze.application.BaseApplication;
import com.michaelscamell.breeze.data.model.ForecastWind;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A fragment that contains a list of forecast items
 */
public class DayFragment extends Fragment implements DayView {

    public static final String ARGS_FORECAST_DATE = "forecast_date";
    public static final String ARGS_CITY_NAME = "city_name";

    @Inject
    DayPresenter presenter;

    @BindView(R.id.forecast_list)
    RecyclerView recyclerView;

    private String cityName;
    private DayForecastAdapter adapter;
    private String forecastDate;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DayFragment() {
    }

    public static DayFragment newInstance(String cityName, String forecastDate) {
        DayFragment dayFragment = new DayFragment();
        Bundle args = new Bundle();
        args.putString(ARGS_CITY_NAME, cityName);
        args.putString(ARGS_FORECAST_DATE, forecastDate);
        dayFragment.setArguments(args);
        return dayFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent(BaseApplication.get(getActivity()).component());
        if (getArguments() != null) {
            cityName = getArguments().getString(ARGS_CITY_NAME);
            forecastDate = getArguments().getString(ARGS_FORECAST_DATE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_day_list, container, false);
        ButterKnife.bind(this, view);
        presenter.onCreate(cityName, forecastDate);
        return view;
    }

    @Override
    public void setupForecast() {
        List<ForecastWind> forecastWinds = Collections.emptyList();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new DayForecastAdapter(forecastWinds);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void updateForecast(List<ForecastWind> forecastWinds) {
        adapter.setForecastWinds(forecastWinds);
    }

    @Override
    public void notifyDataSetChanged() {
        adapter.notifyDataSetChanged();
    }

    private void setupComponent(ApplicationComponent component) {
        DaggerDayComponent.builder()
                .applicationComponent(component)
                .dayModule(new DayModule(this))
                .build()
                .inject(this);
    }

}
