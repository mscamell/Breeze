package com.michaelscamell.breeze.ui.common.LifeCycle;

public interface Pause {
    void onPause();
}
