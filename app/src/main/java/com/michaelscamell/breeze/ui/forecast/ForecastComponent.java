package com.michaelscamell.breeze.ui.forecast;

import com.michaelscamell.breeze.application.ApplicationComponent;
import com.michaelscamell.breeze.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(
        dependencies = ApplicationComponent.class,
        modules = ForecastModule.class
)
public interface ForecastComponent {
    void inject(ForecastActivity forecastActivity);
}
