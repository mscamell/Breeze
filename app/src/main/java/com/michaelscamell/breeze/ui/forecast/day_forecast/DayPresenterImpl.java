package com.michaelscamell.breeze.ui.forecast.day_forecast;

import com.michaelscamell.breeze.data.api.APIClient;
import com.michaelscamell.breeze.data.local.DataManager;
import com.michaelscamell.breeze.data.model.ForecastWind;
import com.michaelscamell.breeze.utils.PostFromAnyThreadBus;

import java.util.List;

import javax.inject.Inject;

public class DayPresenterImpl extends DayPresenter {

    private final APIClient apiClient;
    private final PostFromAnyThreadBus bus;
    private final DataManager dataManager;

    @Inject
    DayPresenterImpl(DayView view, APIClient apiClient, PostFromAnyThreadBus bus, DataManager dataManager) {
        super(view);
        this.apiClient = apiClient;
        this.bus = bus;
        this.dataManager = dataManager;
    }

    @Override
    void onCreate(String cityName, String forecastDate) {
        List<ForecastWind> forecastWinds = dataManager.getForecastWindConditions(cityName, forecastDate);
        view.setupForecast();
        view.updateForecast(forecastWinds);
        view.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        bus.register(this);
    }

    @Override
    public void onPause() {
        bus.unregister(this);
    }

}
