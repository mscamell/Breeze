package com.michaelscamell.breeze.ui.home;

import com.michaelscamell.breeze.ui.common.BasePresenter;
import com.michaelscamell.breeze.ui.common.LifeCycle.Create;
import com.michaelscamell.breeze.ui.common.LifeCycle.Pause;
import com.michaelscamell.breeze.ui.common.LifeCycle.Resume;

/**
 * The DayPresenter for the HomeActivity
 */
public abstract class HomePresenter extends BasePresenter<HomeView> implements Create, Resume, Pause {

    /**
     * Instantiates a new Home presenter.
     *
     * @param view the view
     */
    HomePresenter(HomeView view) {
        super(view);
    }

    /**
     * Adds a favourite city.
     *
     * @param city the city
     */
    abstract void addFavouriteCity(String city);

    /**
     * Handles clearing the favourites from the database
     */
    abstract void clearFavourites();
}
