package com.michaelscamell.breeze.ui.forecast;

import com.michaelscamell.breeze.data.local.DataManager;
import com.michaelscamell.breeze.data.model.ForecastWind;
import com.michaelscamell.breeze.utils.PostFromAnyThreadBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ForecastPresenterImpl extends ForecastPresenter {

    private final PostFromAnyThreadBus bus;
    private final DataManager dataManager;

    @Inject
    ForecastPresenterImpl(ForecastView view, PostFromAnyThreadBus bus, DataManager dataManager) {
        super(view);
        this.bus = bus;
        this.dataManager = dataManager;
    }

    @Override
    void onCreate(String cityName) {
        List<ForecastWind> forecastWinds = dataManager.getForecastWindConditions(cityName);
        view.setupForecast(getTabTitles(forecastWinds));
    }

    private ArrayList<String> getTabTitles(List<ForecastWind> forecastWinds) {
        ArrayList<String> tabTitles = new ArrayList<>();
        for (ForecastWind forecastWind : forecastWinds) {
            if (!tabTitles.contains(forecastWind.getForecastDate())) {
                tabTitles.add(forecastWind.getForecastDate());
            }
        }
        return tabTitles;
    }

    @Override
    public void onResume() {
        bus.register(this);
    }

    @Override
    public void onPause() {
        bus.unregister(this);
    }

}
