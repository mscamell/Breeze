package com.michaelscamell.breeze.ui.forecast;

import com.michaelscamell.breeze.ui.common.BaseView;

import java.util.List;

public interface ForecastView extends BaseView {

    void setupForecast(List<String> tabTitles);
}

