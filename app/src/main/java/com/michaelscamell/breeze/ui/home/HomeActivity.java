package com.michaelscamell.breeze.ui.home;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.michaelscamell.breeze.R;
import com.michaelscamell.breeze.application.ApplicationComponent;
import com.michaelscamell.breeze.data.model.City;
import com.michaelscamell.breeze.ui.common.BaseActivity;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class HomeActivity extends BaseActivity implements HomeView, AddFavouriteCityListener {

    @BindView(R.id.fab)
    FloatingActionButton addFavouriteCityFAB;
    @BindView(R.id.progress)
    ProgressBar progressWheel;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.favourite_cities_empty_text_view)
    TextView emptyMessageTextView;

    @Inject
    HomePresenter homePresenter;

    private FavouriteCityAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        homePresenter.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        homePresenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        homePresenter.onPause();
    }

    @OnClick(R.id.fab)
    public void onAddFavouriteCityFAB() {
        showAddFavouriteCityDialog();
    }

    public void showAddFavouriteCityDialog() {
        AddFavouriteCityDialog addFavouriteCityDialog = AddFavouriteCityDialog.newInstance();
        addFavouriteCityDialog.show(getFragmentManager(), AddFavouriteCityDialog.TAG);
    }

    @Override
    public void onCityEntered(String city) {
        Timber.d("City entered by user for favourites: %s", city);
        homePresenter.addFavouriteCity(city);
    }

    @Override
    public void showLoading() {
        progressWheel.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressWheel.setVisibility(View.GONE);
    }

    @Override
    public void setupRecyclerView() {
        List<City> cities = Collections.emptyList();
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new FavouriteCityAdapter(cities);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void updateFavouriteCities(List<City> cities) {
        adapter.setCities(cities);
    }

    @Override
    public void notifyDataSetChanged() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void hideFavouriteCitiesList() {
        recyclerView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showFavouriteCitiesList() {
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showAddFavCityErrorMessage() {
        Toast.makeText(this, R.string.add_city_failed_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showOfflineMessage() {
        Toast.makeText(this, R.string.currently_offline, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideEmptyListMessage() {
        emptyMessageTextView.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyListMessage() {
        emptyMessageTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_clear_favourites) {
            homePresenter.clearFavourites();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void setupComponent(ApplicationComponent applicationComponent) {
        DaggerHomeComponent.builder()
                .applicationComponent(applicationComponent)
                .homeModule(new HomeModule(this))
                .build()
                .inject(this);
    }

}
