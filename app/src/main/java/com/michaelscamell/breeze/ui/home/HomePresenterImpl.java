package com.michaelscamell.breeze.ui.home;

import com.michaelscamell.breeze.data.api.APIClient;
import com.michaelscamell.breeze.data.local.DataManager;
import com.michaelscamell.breeze.data.model.City;
import com.michaelscamell.breeze.events.ForecastWeatherConditionsSuccessEvent;
import com.michaelscamell.breeze.events.failed.APICallFailEvent;
import com.michaelscamell.breeze.utils.NetworkUtils;
import com.michaelscamell.breeze.utils.PostFromAnyThreadBus;
import com.squareup.otto.Subscribe;

import java.util.List;

import javax.inject.Inject;

public class HomePresenterImpl extends HomePresenter {

    private final APIClient apiClient;
    private final PostFromAnyThreadBus bus;
    private final DataManager dataManager;
    private final NetworkUtils networkUtils;

    @Inject
    HomePresenterImpl(HomeView view,
                      APIClient apiClient,
                      PostFromAnyThreadBus bus,
                      DataManager dataManager,
                      NetworkUtils networkUtils) {
        super(view);
        this.apiClient = apiClient;
        this.bus = bus;
        this.dataManager = dataManager;
        this.networkUtils = networkUtils;
    }

    @Override
    public void onCreate() {
        view.setupRecyclerView();
        List<City> cities = dataManager.getFavourites();
        if (cities != null && cities.size() != 0) {
            view.updateFavouriteCities(cities);
            view.notifyDataSetChanged();
            if (networkUtils.isNetworkAvailable()) {
                for (City city : cities) {
                    apiClient.getCurrentWindCondition(city.getCityName());
                }
            } else {
                view.showOfflineMessage();
            }
        } else {
            view.hideFavouriteCitiesList();
            view.showEmptyListMessage();
        }
    }

    @Override
    public void onResume() {
        bus.register(this);
    }

    @Override
    public void onPause() {
        bus.unregister(this);
    }

    @Override
    void addFavouriteCity(String city) {
        if (networkUtils.isNetworkAvailable()) {
            view.showLoading();
            apiClient.getCurrentWindCondition(city);
        } else {
            view.showOfflineMessage();
        }
    }

    @Override
    void clearFavourites() {
        dataManager.clearFavouriteCities();
        view.notifyDataSetChanged();
        view.showEmptyListMessage();
    }

    /**
     * Subscribe to listen for success events related to getting current wind
     * conditions, hide loading and update RecyclerView
     *
     * @param successEvent the success event
     */
    @Subscribe
    public void getFavouriteCityAddedMessage(ForecastWeatherConditionsSuccessEvent successEvent) {
        view.hideLoading();
        view.hideEmptyListMessage();
        view.showFavouriteCitiesList();
        view.updateFavouriteCities(dataManager.getFavourites());
        view.notifyDataSetChanged();
    }

    /**
     * Subscribe to listen for failure events related to getting current wind
     * conditions and deal with appropriately
     *
     * @param failedEvent that may contain an error status code
     */
    @Subscribe
    public void getCurrentWindConditionsFailedMessage(APICallFailEvent failedEvent) {
        view.hideLoading();
        view.showAddFavCityErrorMessage();
    }
}
