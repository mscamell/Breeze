package com.michaelscamell.breeze.ui.common.LifeCycle;

public interface Destroy {
    void onDestroy();
}
