package com.michaelscamell.breeze.ui.home;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.michaelscamell.breeze.R;
import com.michaelscamell.breeze.data.model.City;
import com.michaelscamell.breeze.ui.forecast.ForecastActivity;
import com.michaelscamell.breeze.utils.StringUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FavouriteCityAdapter extends RecyclerView.Adapter<FavouriteCityAdapter.FavouriteCityViewHolder> {

    private List<City> cities;

    public FavouriteCityAdapter(List<City> cities) {
        this.cities = cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    @Override
    public FavouriteCityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.favourite_city_item, parent, false);
        return new FavouriteCityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FavouriteCityViewHolder holder, int position) {
        final City city = cities.get(position);

        //If possible these would come down from an API and would be more fine grained images
        // tailored to each wind condition. For now it's just 4 based on averages for US state
        // as an example
        int windResourceId = getWindResourceId(city.getCurrentWindConditions().getSpeed());

        Picasso.with(holder.itemView.getContext())
                .load(windResourceId)
                .fit()
                .centerCrop()
                .into(holder.windImage);

        holder.city.setText(city.getCityName());
        holder.windSpeed.setText(StringUtils.speedWithMPH(city.getCurrentWindConditions().getSpeed()));

        //We multiply by minus one so the arrow points in the direction the wind is blowing, rather
        //than pointing to the direction the wind is coming from
        holder.windDirection.setRotation(city.getCurrentWindConditions().getDegrees());
    }

    private int getWindResourceId(float windSpeed) {
        if (windSpeed <= 5) {
            return R.drawable.little_wind;
        } else if (windSpeed <= 10) {
            return R.drawable.light_wind;
        } else if (windSpeed <= 15) {
            return R.drawable.average_wind;
        } else {
            return R.drawable.strong_wind;
        }
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    public static class FavouriteCityViewHolder extends RecyclerView.ViewHolder {

        public static final String KEY_CITY = "city";

        @BindView(R.id.favourite_wind_image)
        ImageView windImage;
        @BindView(R.id.favourite_city)
        TextView city;
        @BindView(R.id.favourite_wind_speed)
        TextView windSpeed;
        @BindView(R.id.favourite_wind_direction)
        ImageView windDirection;

        public FavouriteCityViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.cv)
        public void onFavouriteCityClick() {
            Intent intent = new Intent(itemView.getContext(), ForecastActivity.class);
            intent.putExtra(KEY_CITY, city.getText());
            itemView.getContext().startActivity(intent);
        }
    }
}
