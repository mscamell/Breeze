package com.michaelscamell.breeze.ui.forecast;

import com.michaelscamell.breeze.data.local.DataManager;
import com.michaelscamell.breeze.scope.ActivityScope;
import com.michaelscamell.breeze.utils.PostFromAnyThreadBus;

import dagger.Module;
import dagger.Provides;

@Module
public class ForecastModule {

    private final ForecastView view;

    public ForecastModule(ForecastView view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    public ForecastView provideView() {
        return view;
    }

    @Provides
    @ActivityScope
    public ForecastPresenter providePresenter(ForecastView forecastView, PostFromAnyThreadBus bus, DataManager dataManager) {
        return new ForecastPresenterImpl(forecastView, bus, dataManager);
    }
}
