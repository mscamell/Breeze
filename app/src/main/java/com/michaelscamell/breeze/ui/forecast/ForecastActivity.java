package com.michaelscamell.breeze.ui.forecast;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import com.michaelscamell.breeze.R;
import com.michaelscamell.breeze.application.ApplicationComponent;
import com.michaelscamell.breeze.ui.common.BaseActivity;
import com.michaelscamell.breeze.ui.forecast.day_forecast.DayFragment;
import com.michaelscamell.breeze.ui.forecast.day_forecast.DayPagerAdapter;
import com.michaelscamell.breeze.ui.home.FavouriteCityAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForecastActivity extends BaseActivity implements ForecastView {

    public static final int FORECAST_LENGTH = 5;

    @Inject
    ForecastPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.container)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;

    private DayPagerAdapter dayPagerAdapter;
    private String cityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //get the city name passed through by the FavouriteCityAdapter ViewHolder
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            cityName = extras.getString(FavouriteCityAdapter.FavouriteCityViewHolder.KEY_CITY);
        }

        presenter.onCreate(cityName);
        viewPager.setAdapter(dayPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    /**
     * create the adapter and pass it the day fragments with correct titles
     *
     * @param tabTitles the unique dates for use as tab titles
     */
    @Override
    public void setupForecast(List<String> tabTitles) {
        ArrayList<DayFragment> dayFragments = new ArrayList<>(FORECAST_LENGTH);
        for (int i = 0; i < FORECAST_LENGTH; i++) {
            dayFragments.add(DayFragment.newInstance(cityName, tabTitles.get(i)));
        }
        dayPagerAdapter = new DayPagerAdapter(getSupportFragmentManager(), dayFragments, tabTitles);
    }

    @Override
    protected void setupComponent(ApplicationComponent applicationComponent) {
        DaggerForecastComponent.builder()
                .applicationComponent(applicationComponent)
                .forecastModule(new ForecastModule(this))
                .build()
                .inject(this);
    }

}
