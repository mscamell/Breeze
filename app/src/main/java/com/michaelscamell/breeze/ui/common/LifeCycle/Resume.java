package com.michaelscamell.breeze.ui.common.LifeCycle;

public interface Resume {
    void onResume();
}
