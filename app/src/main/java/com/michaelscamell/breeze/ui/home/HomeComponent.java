package com.michaelscamell.breeze.ui.home;

import com.michaelscamell.breeze.application.ApplicationComponent;
import com.michaelscamell.breeze.scope.ActivityScope;

import dagger.Component;

@ActivityScope
@Component(
        dependencies = ApplicationComponent.class,
        modules = HomeModule.class
)
public interface HomeComponent {
    void inject(HomeActivity homeActivity);
}
