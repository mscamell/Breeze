package com.michaelscamell.breeze.ui.forecast.day_forecast;

import com.michaelscamell.breeze.data.api.APIClient;
import com.michaelscamell.breeze.data.local.DataManager;
import com.michaelscamell.breeze.scope.ActivityScope;
import com.michaelscamell.breeze.utils.PostFromAnyThreadBus;

import dagger.Module;
import dagger.Provides;

@Module
public class DayModule {

    private final DayView view;

    public DayModule(DayView view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    public DayView provideView() {
        return view;
    }

    @Provides
    @ActivityScope
    public DayPresenter providePresenter(DayView dayView, APIClient apiClient, PostFromAnyThreadBus bus, DataManager dataManager) {
        return new DayPresenterImpl(dayView, apiClient, bus, dataManager);
    }
}
