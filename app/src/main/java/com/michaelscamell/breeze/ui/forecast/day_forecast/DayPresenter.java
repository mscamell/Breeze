package com.michaelscamell.breeze.ui.forecast.day_forecast;

import com.michaelscamell.breeze.ui.common.BasePresenter;
import com.michaelscamell.breeze.ui.common.LifeCycle.Pause;
import com.michaelscamell.breeze.ui.common.LifeCycle.Resume;

/**
 * The DayPresenter for the ForecastActivity
 */
public abstract class DayPresenter extends BasePresenter<DayView> implements Pause, Resume {

    /**
     * Instantiates a new forecast presenter.
     *
     * @param view the view
     */
    DayPresenter(DayView view) {
        super(view);
    }

    abstract void onCreate(String cityName, String forecastDate);
}
