package com.michaelscamell.breeze.ui.forecast;

import com.michaelscamell.breeze.ui.common.BasePresenter;
import com.michaelscamell.breeze.ui.common.LifeCycle.Pause;
import com.michaelscamell.breeze.ui.common.LifeCycle.Resume;

/**
 * The DayPresenter for the ForecastActivity
 */
public abstract class ForecastPresenter extends BasePresenter<ForecastView> implements Pause, Resume {

    /**
     * Instantiates a new forecast presenter.
     *
     * @param view the view
     */
    ForecastPresenter(ForecastView view) {
        super(view);
    }

    abstract void onCreate(String cityName);
}
