package com.michaelscamell.breeze.ui.forecast.day_forecast;

import com.michaelscamell.breeze.data.model.ForecastWind;
import com.michaelscamell.breeze.ui.common.BaseView;

import java.util.List;

public interface DayView extends BaseView {

    void setupForecast();

    void updateForecast(List<ForecastWind> forecastWinds);

    void notifyDataSetChanged();
}

