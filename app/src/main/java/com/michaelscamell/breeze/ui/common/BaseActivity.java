package com.michaelscamell.breeze.ui.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.michaelscamell.breeze.application.ApplicationComponent;
import com.michaelscamell.breeze.application.BaseApplication;

/**
 * This should be extended by any activity that needs dependency injection
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent(BaseApplication.get(this).component());
    }

    /**
     * Setup method for dependency injection in the extended activity
     *
     * @param applicationComponent the application component
     */
    protected abstract void setupComponent(ApplicationComponent applicationComponent);
}
