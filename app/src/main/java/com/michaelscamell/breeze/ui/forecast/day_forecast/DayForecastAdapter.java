package com.michaelscamell.breeze.ui.forecast.day_forecast;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.michaelscamell.breeze.R;
import com.michaelscamell.breeze.data.model.ForecastWind;
import com.michaelscamell.breeze.utils.StringUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An adapter that displays the takes forecast winds and presents them as a list
 */
public class DayForecastAdapter extends RecyclerView.Adapter<DayForecastAdapter.ForecastViewHolder> {

    private List<ForecastWind> forecastWinds;

    public DayForecastAdapter(List<ForecastWind> forecastWinds) {
        this.forecastWinds = forecastWinds;
    }

    public void setForecastWinds(List<ForecastWind> forecastWinds) {
        this.forecastWinds = forecastWinds;
    }

    @Override
    public ForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.forecast_item, parent, false);
        return new ForecastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ForecastViewHolder holder, int position) {
        ForecastWind forecastWind = forecastWinds.get(position);

        holder.time.setText(forecastWind.getForecastTime());
        holder.windSpeed.setText(StringUtils.speedWithMPH(forecastWind.getForecastWind().getSpeed()));
        holder.windDirection.setRotation(forecastWind.getForecastWind().getDegrees());
    }

    @Override
    public int getItemCount() {
        return forecastWinds.size();
    }

    public class ForecastViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.forecast_time)
        TextView time;
        @BindView(R.id.forecast_wind_speed)
        TextView windSpeed;
        @BindView(R.id.forecast_wind_direction)
        ImageView windDirection;

        public ForecastViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
