package com.michaelscamell.breeze.ui.home;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.michaelscamell.breeze.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddFavouriteCityDialog extends DialogFragment {

    public static final String TAG = "AddFavouriteCityDialog";

    @BindView(R.id.add_favourite_city_edit_text)
    EditText cityEditText;

    public AddFavouriteCityDialog() {
        //empty constructor
    }

    public static AddFavouriteCityDialog newInstance() {
        return new AddFavouriteCityDialog();
    }

    @SuppressLint("InflateParams")
    @Override
    public AlertDialog onCreateDialog(Bundle savedInstanceState) {
        //Get hold of the listener to return the result to the HomeActivity
        final AddFavouriteCityListener activity = (AddFavouriteCityListener) getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_favourite_city, null);
        ButterKnife.bind(this, view);

        builder.setTitle(R.string.add_favourite_city_title)
                .setView(view)
                .setMessage(R.string.add_favourite_city_message)
                .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.onCityEntered(cityEditText.getText().toString().trim());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
        return builder.create();
    }
}
