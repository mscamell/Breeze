package com.michaelscamell.breeze.ui.home;

import com.michaelscamell.breeze.data.api.APIClient;
import com.michaelscamell.breeze.data.local.DataManager;
import com.michaelscamell.breeze.scope.ActivityScope;
import com.michaelscamell.breeze.utils.NetworkUtils;
import com.michaelscamell.breeze.utils.PostFromAnyThreadBus;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {

    private final HomeView view;

    public HomeModule(HomeView view) {
        this.view = view;
    }

    @Provides
    @ActivityScope
    public HomeView provideView() {
        return view;
    }

    @Provides
    @ActivityScope
    public HomePresenter providePresenter(HomeView homeView,
                                          APIClient apiClient,
                                          PostFromAnyThreadBus bus,
                                          DataManager dataManager,
                                          NetworkUtils networkUtils) {
        return new HomePresenterImpl(homeView, apiClient, bus, dataManager, networkUtils);
    }
}
