package com.michaelscamell.breeze.application;

import timber.log.Timber;

public class DebugApplication extends BaseApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
    }

}
