package com.michaelscamell.breeze;

import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.michaelscamell.breeze.data.model.City;
import com.michaelscamell.breeze.ui.home.HomeActivity;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import io.realm.Realm;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

/**
 * A test of the typical flow through the Home Activity. WARNING! This will delete the data in the
 * database on start up so that the test has a typical new user flow.
 */
@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@LargeTest
public class HomeActivityUITest {

    @Rule
    public ActivityTestRule<HomeActivity> mActivityRule = new ActivityTestRule<>(
            HomeActivity.class);
    private String city;

    @Before
    public void setup() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.clear(City.class);
        realm.commitTransaction();
        city = "London";
    }

    @Test
    public void test1_NoCities_EmptyListMessageDisplayed() {
        onView(withId(R.id.favourite_cities_empty_text_view))
                .check(ViewAssertions.matches(isDisplayed()));
    }

    @Test
    public void test2_OnFabClick_EnterFavouriteCityDialogDisplayed() {
        onView(withId(R.id.fab))
                .perform(click());

        onView(withId(R.id.add_favourite_city_edit_text))
                .check(ViewAssertions.matches(isDisplayed()));

    }

    @Test
    public void test3_OnFavCityEntered_FavCitiesInRecyclerView() {
        onView(withId(R.id.fab))
                .perform(click());

        onView(withId(R.id.add_favourite_city_edit_text))
                .perform(typeText(city), closeSoftKeyboard());

        //Click the OK button
        onView(withId(android.R.id.button1)).perform(click());

        onView(withId(R.id.recycler_view))
                .perform(RecyclerViewActions.scrollToPosition(0));

        onView(withId(R.id.favourite_city))
                .check(ViewAssertions.matches(withText(city)));

    }

    @Test
    public void test4_OnFavCityInList_NoEmptyListMessage() {
        onView(withId(R.id.favourite_cities_empty_text_view))
                .check(ViewAssertions.matches(not(isDisplayed())));
    }

}